/*
 * Funcionalidad de tu producto
 */
var programadorasImagens = document.getElementById("programadorasImagens");
programadorasImagens.addEventListener("change", carregarImagensProgramadoras);

function carregarImagensProgramadoras() {
  var sede = programadorasImagens.value;
  var divPrincipal = document.getElementById("divPrincipal");
  divPrincipal.innerHTML = "";

  for (turma in data[sede]) {
    for (i in data[sede][turma]["students"]) {
      var aluno = data[sede][turma]["students"][i];

      var divCartao = document.createElement("div");
      // inserir a imagem no cartao
      var imagemFotoAluno = document.createElement("img");
      imagemFotoAluno.src = aluno.photo;
      // atribui a imagem a div criada
      divCartao.appendChild(imagemFotoAluno);
      // atibui o card criado a div principal
      divPrincipal.appendChild(divCartao);

      // nome das alunas
      var divNome = document.createElement("div");
      var nome = document.createElement("p");
      nome.innerHTML = aluno.name;
      divNome.appendChild(nome);
      divPrincipal.appendChild(divNome);

      // DIV ALUNAS ATIVAS E INATIVAS
      var divAlunasAtivas = document.createElement("div");
      var status = document.createElement("p");
      divAlunasAtivas.appendChild(status);
      divPrincipal.appendChild(divAlunasAtivas);
      if (aluno.active === true) {
        status.innerHTML = "Ativo";
      } else {
        status.innerHTML = "Inativo";
      }

      for (j in aluno.sprints) {
        var sprintDoAluno = aluno.sprints[j];

        // numero do sprint
        // var divNumeroSprint = document.createElement('div');
        // var pNumeroSprint = document.createElement('p');
        // pNumeroSprint.innerHTML = 'SPRINT' + " " + sprintDoAluno.number;
        // divNumeroSprint.appendChild(pNumeroSprint);
        // divPrincipal.appendChild(divNumeroSprint);
        // pontuacao tecnica

        // var divPontosTech = document.createElement('div');
        // var pPontosTech = document.createElement('p');
        // pPontosTech.innerHTML = "TECH" + " " + sprintDoAluno.score.tech;
        // divPontosTech.appendChild(pPontosTech);
        // divPrincipal.appendChild(divPontosTech);

        // pontuacao HSE
        // var divPontuacaoHse = document.createElement('div');
        // var pPontuacaoHse = document.createElement('p');
        // pPontuacaoHse.innerHTML = "HSE" + " " + sprintDoAluno.score.hse;
        // divPontuacaoHse.appendChild(pPontuacaoHse);
        // divPrincipal.appendChild(divPontuacaoHse);
      }
    }
  }
}
// ATIVOS E INATIVOS AREQUIPA
var ativos = [];

for (var i = 0; i < data.AQP["2016-2"].students.length; i++) {
  ativos.push(data.AQP["2016-2"].students[i].active);
  var contaAlunasAtivas = ativos.reduce(function(todasAlunas, alunasAtivas) {
    if (alunasAtivas in todasAlunas) {
      todasAlunas[alunasAtivas]++;
    } else {
      todasAlunas[alunasAtivas] = 1;
    }
    return todasAlunas;
  }, {});
}

var ativosLima = [];

for (var i = 0; i < data.LIM["2016-2"].students.length; i++) {
  ativosLima.push(data.LIM["2016-2"].students[i].active);
  var contaAlunasAtivasLima = ativosLima.reduce(function(
    todasAlunasLim,
    alunasAtivasLim
  ) {
    if (alunasAtivasLim in todasAlunasLim) {
      todasAlunasLim[alunasAtivasLim]++;
    } else {
      todasAlunasLim[alunasAtivasLim] = 1;
    }
    return todasAlunasLim;
  },
  {});
}
var ativosScl = [];

for (var i = 0; i < data.SCL["2016-2"].students.length; i++) {
  ativosScl.push(data.SCL["2016-2"].students[i].active);
  var contaAlunasScl = ativosScl.reduce(function(
    todasAlunasScl,
    alunasAtivasScl
  ) {
    if (alunasAtivasScl in todasAlunasScl) {
      todasAlunasScl[alunasAtivasScl]++;
    } else {
      todasAlunasScl[alunasAtivasScl] = 1;
    }
    return todasAlunasScl;
  },
  {});
}
google.charts.load("current", { packages: ["corechart", "bar"] });
google.charts.setOnLoadCallback(drawAxisTickColors);

function drawAxisTickColors() {
  var data = google.visualization.arrayToDataTable([
    ["CIDADE", "2016-2 ATIVAS", "2016-2 INATIVAS"],
    ["AREQUIPA", contaAlunasAtivas.true, contaAlunasAtivas.false],
    ["LIMA", contaAlunasAtivasLima.true, contaAlunasAtivasLima.false],
    ["CIDADE DO MEXICO", 0, 0],
    ["SANTIAGO", contaAlunasScl.true, contaAlunasScl.false]
  ]);

  var options = {
    title: "TOTAL DE ALUNAS ATIVAS/INATIVAS",
    chartArea: { width: "50%" },
    hAxis: {
      title: "Total alunas",
      minValue: 0,
      textStyle: {
        bold: true,
        fontSize: 12,
        color: "#4d4d4d"
      },
      titleTextStyle: {
        bold: true,
        fontSize: 18,
        color: "#4d4d4d"
      }
    },
    vAxis: {
      title: "City",
      textStyle: {
        fontSize: 14,
        bold: true,
        color: "#848484"
      },
      titleTextStyle: {
        fontSize: 14,
        bold: true,
        color: "#848484"
      }
    }
  };
  var chart = new google.visualization.BarChart(
    document.getElementById("chart_div")
  );
  chart.draw(data, options);
}

// media jedi
var mediaJedi = [];


for (var i = 0; i < data.AQP["2016-2"].ratings.length; i++) {
  mediaJedi.push(data.AQP["2016-2"].ratings[i].jedi);
}
var resultSomaJedi = mediaJedi.reduce(function(acumulador, valorAtual) {
  return acumulador + valorAtual;
});

var resultMediaJedi = resultSomaJedi / mediaJedi.length;

var porcentagenJedi = Math.round(resultMediaJedi * 100) / 5;
var mediaJediHtml = document.getElementById("mediaJediHtml").innerHTML = "MÉDIA JEDI = " + porcentagenJedi + "%";
// media teacher
var mediaTeacher = [];

for (var i = 0; i < data.AQP["2016-2"].ratings.length; i++) {
  mediaTeacher.push(data.AQP["2016-2"].ratings[i].teacher);
}
var resultSomaTeacher = mediaTeacher.reduce(function(acumulador, valorAtual) {
  return acumulador + valorAtual;
});

var resultMediaTeacher = resultSomaTeacher / mediaTeacher.length;

var porcentagenTeacher = Math.round(resultMediaTeacher * 100) / 5;
var mediateacherHtml = document.getElementById("mediateacherHtml").innerHTML = "MÉDIA TEACHER = " + porcentagenTeacher + "%";
google.charts.load("current", { packages: ["corechart"] });
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ["Year", "JEDI", "TEACHER"],
    ["Sprint 1", mediaJedi[0], mediaTeacher[0]],
    ["Sprint 2", mediaJedi[1], mediaTeacher[1]],
    ["Sprint 3", mediaJedi[2], mediaTeacher[2]],
    ["Sprint 4", mediaJedi[3], mediaTeacher[3]]
  ]);

  var options = {
    title: "Média Jedi/Teacher AREQUIPA",
    curveType: "function",
    legend: { position: "bottom" }
  };

  var chart = new google.visualization.LineChart(
    document.getElementById("curve_chart")
  );

  chart.draw(data, options);
}

// promoters
var promoters = [];
var detractors = [];
var resultadoNps = [];

for (var i = 0; i < data.AQP["2016-2"].ratings.length; i++) {
  promoters.push(data.AQP["2016-2"].ratings[i].nps.promoters);
  detractors.push(data.AQP["2016-2"].ratings[i].nps.detractors);
  resultadoNps.push(promoters[i] - detractors[i]);
}
var nPs = resultadoNps.reduce(function(acumul, valor){
return acumul + valor;
});
var mediaNps = nPs/resultadoNps.length;
var mediaNpsHml = document.getElementById("mediaNpsHtml").innerHTML = "MÉDIA NPS = " + mediaNps + "%";



// var resultSomaTeacher = mediaTeacher.reduce(function(acumulador, valorAtual) {
//   return acumulador + valorAtual;
// // });

google.charts.load('current', {'packages':['bar']});
google.charts.setOnLoadCallback(drawStuff);
function drawStuff() {
  var data = new google.visualization.arrayToDataTable([
    ['NPS', 'Detractors', 'Promotors'],
    ['Sprint 1', detractors[0], promoters[0]],
    ['Sprint 2', detractors[1], promoters[1]],
    ['Sprint 3', detractors[2], promoters[2]],
    ['Sprint 4', detractors[3], promoters[3]],
    
  ]);

  var options = {
    width: 800,
    chart: {
      
      
    },
    bars: 'horizontal', // Required for Material Bar Charts.
    series: {
      0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
      1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
    },
    axes: {
      x: {
        distance: {label: ''}, // Bottom x-axis.
        brightness: {side: 'top', label: 'NPS sprints AREQUIPA'} // Top x-axis.
      }
    }
  };

var chart = new google.charts.Bar(document.getElementById('dual_x_div'));
chart.draw(data, options);
};

// Puedes hacer uso de la base de datos a través de la variable `data`
console.log(data);
